package fr.univsmb.monopoly.cases;


public abstract class Case {
    protected int id;

    public Case(int id) {
        this.id = id;
    }

    public Case() {
    }

    public void getEffect() {
    }

}
