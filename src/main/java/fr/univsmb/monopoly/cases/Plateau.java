package fr.univsmb.monopoly.cases;

import java.util.ArrayList;


public class Plateau {
    ArrayList<Case> listeCases = new ArrayList<Case>();

    void setPlateau(){
        for(int i=0; i<40 ; i++)
        {
            if(i==0)
                listeCases.add(new Depart(i));
            else if(i==4)
                listeCases.add(new Impots(i));
            else if(i==38)
                listeCases.add(new TaxeDeLuxe(i));
            //else if((i>0 && i<4) || (i>4 && i<38) || (i>38 && i<40))
            else
                listeCases.add(new CaseNonPossedable(i));


        }

    }
}
